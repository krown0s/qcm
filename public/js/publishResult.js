$(document).ready(function() {
    $("button[id^='publishResult']").click(function() {
        publishResult(this);
    });
});

function publishResult(index) {
    var idPublishResult = jQuery(index).val();

    $.ajax({
        //On utilise le type GET
        type: 'POST',

        //Ici on appelle la fonction CreateQcm du controleur QuestionReponseControlleur.php
        url: "showResults",

        //On passe en paramètre le message et l'auteur du message
        data: {
            publishResult: idPublishResult
        },

        // code_html contient le HTML renvoyé
        success : function(code_html, statut){
            $("#createdMessage" + idPublishResult).text("La note est maintenant publiée.");
            $("#createdMessage" + idPublishResult).attr('class', 'text-success');
        }
    });

}