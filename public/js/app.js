function addResponse() {
    var formCreateQuestion = document.getElementById("responses");
    var nbInputResponse = formCreateQuestion.getElementsByTagName("input");

    //Calcul du nombre d'input pour le nommage des elements
    nbInputResponse = nbInputResponse.length / 2 + 1;
    var inputResponse = document.createElement("input");

    //Creation d'attributs
    var typeResponse = document.createAttribute("type");
    typeResponse.value="text";

    var classType = document.createAttribute("class");
    classType.value = "form-control";

    var idResponse = document.createAttribute("id");
    idResponse.value= "response" + nbInputResponse;

    var nameResponse = document.createAttribute("name");
    nameResponse.value = "response" + nbInputResponse;

    //On applique les attributs à notre response
    inputResponse.setAttributeNode(typeResponse);
    inputResponse.setAttributeNode(nameResponse);
    inputResponse.setAttributeNode(classType);
    inputResponse.setAttributeNode(idResponse);

    formCreateQuestion.appendChild(inputResponse);

    var inputResponseTrueFalse = document.createElement("input");

    var typeResponseTrueFalse = document.createAttribute("type");
    typeResponseTrueFalse.value="checkbox";

    var nameResponseTrueFalse = document.createAttribute("name");
    nameResponseTrueFalse.value = "response" + nbInputResponse + "TrueFalse";

    var idResponseTrueFalse = document.createAttribute("id");
    idResponseTrueFalse.value = "response" + nbInputResponse + "TrueFalse";

    inputResponseTrueFalse.setAttributeNode(typeResponseTrueFalse);
    inputResponseTrueFalse.setAttributeNode(nameResponseTrueFalse);
    inputResponseTrueFalse.setAttributeNode(idResponseTrueFalse);

    formCreateQuestion.appendChild(inputResponseTrueFalse);
}

function addResponseUpdatedQuestion() {
    var divcol9 = document.createElement('div');
    divcol9.setAttribute('class', 'col-9');

    var divRow = document.createElement('div');
    divRow.setAttribute('class', 'row');

    var divFormGroup = document.createElement('div');
    divFormGroup.setAttribute('class', 'form-group');

    var divInputGroup = document.createElement('div');
    divInputGroup.setAttribute('class', 'input-group');

    var divcol3 = document.createElement('div');
    divcol3.setAttribute('class', 'col-3');

    var formCreateQuestion = document.getElementById("responses");
    var nbInputResponse = formCreateQuestion.getElementsByTagName("input");

    //Calcul du nombre d'input pour le nommage des elements
    nbInputResponse = nbInputResponse.length / 2 + 1;
    var inputResponse = document.createElement("input");

    //Creation d'attributs
    var typeResponse = document.createAttribute("type");
    typeResponse.value="text";

    var classType = document.createAttribute("class");
    classType.value = "form-control";

    var idResponse = document.createAttribute("id");
    idResponse.value= "response" + nbInputResponse;

    var nameResponse = document.createAttribute("name");
    nameResponse.value = "response" + nbInputResponse;

    //On applique les attributs à notre response
    inputResponse.setAttributeNode(typeResponse);
    inputResponse.setAttributeNode(nameResponse);
    inputResponse.setAttributeNode(classType);
    inputResponse.setAttributeNode(idResponse);

    var inputResponseTrueFalse = document.createElement("input");

    var typeResponseTrueFalse = document.createAttribute("type");
    typeResponseTrueFalse.value="checkbox";

    var nameResponseTrueFalse = document.createAttribute("name");
    nameResponseTrueFalse.value = "response" + nbInputResponse + "TrueFalse";


    var classResponseTrueFalse = document.createAttribute('class');
    classResponseTrueFalse.value="form-check-label";

    var idResponseTrueFalse = document.createAttribute("id");
    idResponseTrueFalse.value = "response" + nbInputResponse + "TrueFalse";

    inputResponseTrueFalse.setAttributeNode(typeResponseTrueFalse);
    inputResponseTrueFalse.setAttributeNode(nameResponseTrueFalse);
    inputResponseTrueFalse.setAttributeNode(idResponseTrueFalse);
    inputResponseTrueFalse.setAttributeNode(classResponseTrueFalse);

    divRow.appendChild(divcol9);
    divRow.appendChild(divcol3);
    formCreateQuestion.appendChild(divRow)


    divcol9.appendChild(divFormGroup);
    divcol3.appendChild(divInputGroup);
    divFormGroup.appendChild(inputResponse);
    divInputGroup.appendChild(inputResponseTrueFalse);
}
