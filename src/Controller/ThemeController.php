<?php

namespace App\Controller;

use App\Entity\Theme;
use App\Entity\Question;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Annotation\Route;

class ThemeController extends AbstractController
{

    /**
     * Redirection sur la page d'accueil
     * @Route("/updateTheme/")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function redirectUpdateTheme()
    {
        return $this->redirect('/');
    }

    /**
     * @Route("/showThemes", name="showThemes")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function showThemes(Request $request)
    {
        $session = new Session();
        if(User::checkSession($request, $session))
        {
            if($request->getSession()->get('role') == 'Enseignant') {
                $repository = $this->getDoctrine()->getRepository(Theme::class);
                $themes = $repository->findAll();
                return $this->render('/teacher/showThemes.html.twig', ["themes" => $themes]);
            } else {
                $this->get('session')->clear();

                return $this->redirect('/');
            }
        }
        else
        {
            $erreur = array('erreur' => "Veuillez vous authentifier");
            return $this->render('home.html.twig', $erreur);
        }
    }

    /**
     * @Route("/createTheme", name="createTheme")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createTheme(Request $request)
    {
        $session = new Session();
        if(User::checkSession($request, $session))
        {
            if($request->getSession()->get('role') == 'Enseignant') {
                return $this->render('/teacher/createTheme.html.twig');
            } else {
                $this->get('session')->clear();

                return $this->redirect('/');
            }
        }
        else
        {
            $erreur = array('erreur' => "Veuillez vous authentifier");
            return $this->render('home.html.twig', $erreur);
        }
    }

    /**
     * @Route("createThemeDB", name="createThemeDB")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function createThemeDB(Request $request)
    {
        $session = new Session();
        if(User::checkSession($request, $session))
        {
            if($request->getSession()->get('role') == 'Enseignant') {
                $theme = $request->get('theme');
                if(isset($theme) && !empty($theme))
                {
                    $entityManager = $this->getDoctrine()->getManager();
                    $themeDB = new Theme();
                    $themeDB->setContent($theme);
                    $entityManager->persist($themeDB);
                    $entityManager->flush();
                    return $this->redirect('/showThemes');
                }
                else
                {
                    return $this->redirect('/showThemes');
                }
            } else {
                $this->get('session')->clear();

                return $this->redirect('/');
            }

        }
        else
        {
            $erreur = array('erreur' => "Veuillez vous authentifier");
            return $this->render('home.html.twig', $erreur);
        }
    }

    /**
     * @Route("/deleteTheme/{idTheme}", name="deleteTheme")
     * @param $idTheme
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function deleteTheme($idTheme, Request $request)
    {
        $session = new Session();
        if(User::checkSession($request, $session))
        {
            if($request->getSession()->get('role') == 'Enseignant') {
                $repository = $this->getDoctrine()->getRepository(Theme::class);
                $repositoryQuestion = $this->getDoctrine()->getRepository(Question::class);
                if(sizeof($repositoryQuestion->findBy(['theme' => $idTheme])) > 0) {
                    $this->addFlash('danger', 'Theme non supprimé car il est utilisée dans une question!');
                    return $this->redirect('/showThemes');

                } else {
                    $themeDB = $repository->find($idTheme);
                    $entityManager = $this->getDoctrine()->getManager();

                    // Suppression du theme d'id "$idTheme"
                    $entityManager->remove($themeDB);
                    $entityManager->flush();
                    $this->addFlash('success', 'Theme supprimé!');
                    return $this->redirect('/showThemes');
                }
            } else {
                $this->get('session')->clear();

                return $this->redirect('/');
            }
        }
        else
        {
            $erreur = array('erreur' => "Veuillez vous authentifier");
            return $this->render('home.html.twig', $erreur);
        }
    }

    /**
     * @Route("/updateTheme/{idTheme}", name="updateTheme")
     * @param $idTheme
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateTheme($idTheme, Request $request)
    {
        $session = new Session();
        if(User::checkSession($request, $session))
        {
            if($request->getSession()->get('role') == 'Enseignant') {
                $repository = $this->getDoctrine()->getRepository(Theme::class);
                $theme = $repository->find($idTheme);
                return $this->render('/teacher/updateTheme.html.twig', array("theme" => $theme));
            } else {
                $this->get('session')->clear();

                return $this->redirect('/');
            }
        }
        else
        {
            $erreur = array('erreur' => "Veuillez vous authentifier");
            return $this->render('home.html.twig', $erreur);
        }
    }

    /**
     * @Route("/updateThemeDB", name="updateThemeDB")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateThemeDB(Request $request)
    {
        $session = new Session();
        if(User::checkSession($request, $session))
        {
            if($request->getSession()->get('role') == 'Enseignant') {
                $theme = $request->get('theme');
                $idTheme = $request->get('idTheme');
                // Si les informations relatives au theme ont été saisies dans les champs
                if(isset($theme) && !empty($theme) && isset($idTheme) && !empty($idTheme))
                {
                    $entityManager = $this->getDoctrine()->getManager();
                    $repository = $this->getDoctrine()->getRepository(Theme::class);

                    // Récupération du theme existant, et on le modifie
                    $themeDB = $repository->find($idTheme);

                    $themeDB->setContent($theme);

                    $entityManager->flush();
                }
                return $this->redirect('/showThemes');
            } else {
                $this->get('session')->clear();

                return $this->redirect('/');
            }

        }
        else
        {
            $erreur = array('erreur' => "Veuillez vous authentifier");
            return $this->render('home.html.twig', $erreur);
        }
    }

}