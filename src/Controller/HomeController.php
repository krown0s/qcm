<?php

namespace App\Controller;

use App\Entity\Qcm;
use App\Entity\Question;
use App\Entity\Response;
use App\Entity\Result;
use App\Entity\Theme;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use App\Entity\User;


class HomeController extends AbstractController
{

    /**
     * @Route("/", name="HomeController")
     */
    public function home(Request $request)
    {
        $session = new Session();
        // Si la session existe
        if(User::checkSession($request, $session))
        {
            /** @var User $user */
            $user = $this->getDoctrine()->getRepository(User::Class)->findOneByLogin($session->get('username'));
            $userRole = $request->getSession()->get('role');
            // Si on est étudiant
            if($userRole == "Etudiant")
            {
                $repositoryResult = $this->getDoctrine()->getRepository(Result::class);
                $idUser = $request->getSession()->get('idUser');
                $results = $repositoryResult->findBy(
                    ['visible' => '1', 'user_id' => $idUser]
                );
                $nombreNote = count($results);
                $moyenne = null;

                // Calcul de la moyenne de l'etudiant
                if(!$nombreNote == 0)
                {
                    foreach($results as $r)
                    {
                        $moyenne = $r->getResult() + $moyenne;
                    }
                    $moyenne = number_format(($moyenne / $nombreNote), 2);
                }

                // Recuperation du nombre de qcm auxquels l'etudiant a repondu
                $nombreQcm = count($repositoryResult->findBy(['user_id' => $idUser]));

                // Stockage des informations de l'etudiants qu'on affichera sur sa page d'accueil
                $userInf =
                    array('username' => $user->getLogin(),
                        'password' => $user->getPassword(),
                        'role' => $user->getRole(),
                        'email' => $user->getEmail(),
                        'userRole' => $userRole,
                        'results' => $moyenne,
                        'nqcm' => $nombreQcm);
            }
            else
            {
                $students = $this->getDoctrine()->getRepository(User::class)->findByRole('Etudiant');
                $teachers = $this->getDoctrine()->getRepository(User::class)->findByRole('Enseignant');
                $themes = $this->getDoctrine()->getRepository(Theme::class)->findAll();
                $qcms = $this->getDoctrine()->getRepository(Qcm::class)->findAll();

                // Stockage des informations de l'enseignant qu'on affichera sur sa page d'accueil
                $userInf =
                    array('username' => $user->getLogin(),
                        'password' => $user->getPassword(),
                        'role' => $user->getRole(),
                        'email' => $user->getEmail(),
                        'userRole' => $userRole,
                        'students' => $students,
                        'teachers' => $teachers,
                        'themes' => $themes,
                        'qcms' => $qcms);
            }
            return $this->render('/teacher/test.html.twig', $userInf);
        }
        else
        {
            $erreur = array('erreur' => "");
            return $this->render('home.html.twig', $erreur);
        }
    }

}